This TA is a TEE implementation of OpenHarmony's pin auth functionality.

Note that: if the TA is to be used in production environment, please reimplement the device dependent functions `DeriveDeviceKey()` in `pin_auth/src/adaptor/src/adaptor_algorithm.c` and `GetDevUdid()` in `pin_auth/src/pin_func/pin_auth.c`.