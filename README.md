# tee_tee_ta

#### 介绍
提供运行在TEE环境中的可信应用，如口令认证TA。

#### 软件架构
软件架构说明
![输入图片说明](TA.png)

#### 目录
base/tee/tee_ta

├── pin_auth           # 口令认证TA

#### 相关仓
[tee_tee_os_framework](https://gitee.com/openharmony-sig/tee_tee_os_framework)

[tee_tee_os_kernel](https://gitee.com/openharmony-sig/tee_tee_os_kernel)

[tee_tee_dev_kit](https://gitee.com/openharmony-sig/tee_tee_dev_kit)

[useriam_pin_auth](http://gitee.com/openharmony/useriam_pin_auth)